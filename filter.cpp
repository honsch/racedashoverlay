#include "filter.h"


//-----------------------------------------------------------------------
void Filter::Fill(double *state, double value, unsigned count)
{
    unsigned a;
    for (a = 0; a < count; a++) state[a] = value;
}

//---------------------------------------------
Butterworth_4th_10_100::Butterworth_4th_10_100(double fill)
{
    Fill(mState, fill, 10);
}

//---------------------------------------------
Butterworth_4th_1_100::Butterworth_4th_1_100(double fill)
{
    Fill(mState, fill, 10);
}

//---------------------------------------------
Bessel_4th_15_100::Bessel_4th_15_100(double fill)
{
    Fill(mState, fill, 10);
}

//---------------------------------------------
Bessel_4th_10_100::Bessel_4th_10_100(double fill)
{
    Fill(mState, fill, 10);
}

//---------------------------------------------
Bessel_4th_1_100::Bessel_4th_1_100(double fill)
{
    Fill(mState, fill, 10);
}

//---------------------------------------------
Bessel_6th_25_100::Bessel_6th_25_100(double fill)
{
    Fill(mState, fill, 14);
}

//---------------------------------------------
Bessel_6th_20_100::Bessel_6th_20_100(double fill)
{
    Fill(mState, fill, 14);
}

//---------------------------------------------
Bessel_6th_15_100::Bessel_6th_15_100(double fill)
{
    Fill(mState, fill, 14);
}

//---------------------------------------------
Bessel_6th_10_100::Bessel_6th_10_100(double fill)
{
    Fill(mState, fill, 14);
}

//---------------------------------------------
Bessel_6th_5_100::Bessel_6th_5_100(double fill)
{
    Fill(mState, fill, 14);
}



//-----------------------------------------------------------------------
// Fourth order butterworth, -3db @ 0.1 * sample rate
double Butterworth_4th_10_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = newSample / 2.072820954e+02;
    mState[5] = mState[6];
    mState[6] = mState[7];
    mState[7] = mState[8];
    mState[8] = mState[9];
    mState[9] = (mState[0] + mState[4]) + 4 * (mState[1] + mState[3]) + 6 * mState[2]
               + (-0.1873794924 * mState[5]) + (1.0546654059 * mState[6])
               + (-2.3139884144 * mState[7]) + (2.3695130072 * mState[8]);
    return mState[9];
}


//-----------------------------------------------------------------------
double Butterworth_4th_1_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = newSample / 1.112983215e+06;
    mState[5] = mState[6];
    mState[6] = mState[7];
    mState[7] = mState[8];
    mState[8] = mState[9];
    mState[9] = (mState[0] + mState[4]) + 4 * (mState[1] + mState[3]) + 6 * mState[2]
               + (-0.8485559993 * mState[5]) + (3.5335352195 * mState[6])
               + (-5.5208191366 * mState[7]) + (3.8358255406 * mState[8]);
    return mState[9];
}

//-----------------------------------------------------------------------
double Bessel_4th_15_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = newSample / 2.214828640e+01;
    mState[5] = mState[6];
    mState[6] = mState[7];
    mState[7] = mState[8];
    mState[8] = mState[9];
    mState[9] = (mState[0] + mState[4]) + 4 * (mState[1] + mState[3]) + 6 * mState[2]
               + (-0.0112221588 * mState[5]) + (0.0894858200 * mState[6])
               + (-0.3691558950 * mState[7]) + (0.5684887165 * mState[8]);
    return mState[9];
}

//-----------------------------------------------------------------------
double Bessel_4th_10_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = newSample / 6.893641214e+01;
    mState[5] = mState[6];
    mState[6] = mState[7];
    mState[7] = mState[8];
    mState[8] = mState[9];
    mState[9] = (mState[0] + mState[4]) + 4 * (mState[1] + mState[3]) + 6 * mState[2]
               + (-0.0503462932 * mState[5]) + (0.3599070274 * mState[6])
               + (-1.0458620167 * mState[7]) + (1.5042033315 * mState[8]);
    return mState[9];
}

//-----------------------------------------------------------------------
double Bessel_4th_1_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = newSample / 2.259747797e+05;
    mState[5] = mState[6];
    mState[6] = mState[7];
    mState[7] = mState[8];
    mState[8] = mState[9];
    mState[9] = (mState[0] + mState[4]) + 4 * (mState[1] + mState[3]) + 6 * mState[2]
               + (-0.7428578687 * mState[5]) + (3.1954036268 * mState[6])
               + (-5.1599230905 * mState[7]) + (3.7073065280 * mState[8]);
    return mState[9];
}

//-----------------------------------------------------------------------
double Bessel_6th_25_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = mState[5];
    mState[5] = mState[6];
    mState[6] = newSample / 1.082848383e+01;

    mState[0 + 7] = mState[1 + 7];
    mState[1 + 7] = mState[2 + 7];
    mState[2 + 7] = mState[3 + 7];
    mState[3 + 7] = mState[4 + 7];
    mState[4 + 7] = mState[5 + 7];
    mState[5 + 7] = mState[6 + 7];
    mState[6 + 7] = (mState[0] + mState[6]) + 6 * (mState[1] + mState[5]) + 15 * (mState[2] + mState[4]) + 20 * mState[3]
                     + (-0.0044906294 * mState[0 + 7]) + ( -0.0566854160 * mState[1 + 7])
                     + (-0.3113334558 * mState[2 + 7]) + ( -0.9490871236 * mState[3 + 7])
                     + (-1.7503173268 * mState[4 + 7]) + ( -1.8384240979 * mState[5 + 7]);

    return mState[6 + 7];
}

//-----------------------------------------------------------------------
double Bessel_6th_20_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = mState[5];
    mState[5] = mState[6];
    mState[6] = newSample / 2.282725061e+01;

    mState[0 + 7] = mState[1 + 7];
    mState[1 + 7] = mState[2 + 7];
    mState[2 + 7] = mState[3 + 7];
    mState[3 + 7] = mState[4 + 7];
    mState[4 + 7] = mState[5 + 7];
    mState[5 + 7] = mState[6 + 7];
    mState[6 + 7] = (mState[0] + mState[6]) + 6 * (mState[1] + mState[5]) + 15 * (mState[2] + mState[4]) + 20 * mState[3]
                     + (-0.0006482767 * mState[0 + 7]) + ( -0.0083102858 * mState[1 + 7])
                     + (-0.0695694301 * mState[2 + 7]) + ( -0.2282693437 * mState[3 + 7])
                     + (-0.6895164410 * mState[4 + 7]) + ( -0.8073528180 * mState[5 + 7]);

    return mState[6 + 7];
}

//-----------------------------------------------------------------------
double Bessel_6th_15_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = mState[5];
    mState[5] = mState[6];
    mState[6] = newSample / 6.385476923e+01;

    mState[0 + 7] = mState[1 + 7];
    mState[1 + 7] = mState[2 + 7];
    mState[2 + 7] = mState[3 + 7];
    mState[3 + 7] = mState[4 + 7];
    mState[4 + 7] = mState[5 + 7];
    mState[5 + 7] = mState[6 + 7];
    mState[6 + 7] = (mState[0] + mState[6]) + 6 * (mState[1] + mState[5]) + 15 * (mState[2] + mState[4]) + 20 * mState[3]
                     + (-0.0007378695 * mState[0 + 7]) + ( 0.0086127014 * mState[1 + 7])
                     + (-0.0579335270 * mState[2 + 7]) + ( 0.1576523947 * mState[3 + 7])
                     + (-0.5179250427 * mState[4 + 7]) + ( 0.4080569512 * mState[5 + 7]);

    return mState[6 + 7];
}

//-----------------------------------------------------------------------
double Bessel_6th_10_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = mState[5];
    mState[5] = mState[6];
    mState[6] = newSample / 3.198759421e+02;

    mState[0 + 7] = mState[1 + 7];
    mState[1 + 7] = mState[2 + 7];
    mState[2 + 7] = mState[3 + 7];
    mState[3 + 7] = mState[4 + 7];
    mState[4 + 7] = mState[5 + 7];
    mState[5 + 7] = mState[6 + 7];
    mState[6 + 7] = (mState[0] + mState[6]) + 6 * (mState[1] + mState[5]) + 15 * (mState[2] + mState[4]) + 20 * mState[3]
                     + (-0.0078390522 * mState[0 + 7]) + (  0.0852096278 * mState[1 + 7])
                     + (-0.4080412916 * mState[2 + 7]) + (  1.1157139955 * mState[3 + 7])
                     + (-1.8767603680 * mState[4 + 7]) + (  1.8916395224 * mState[5 + 7]);

    return mState[6 + 7];
}

//-----------------------------------------------------------------------
double Bessel_6th_5_100::NewSample(double newSample)
{
    mState[0] = mState[1];
    mState[1] = mState[2];
    mState[2] = mState[3];
    mState[3] = mState[4];
    mState[4] = mState[5];
    mState[5] = mState[6];
    mState[6] = newSample / 7.773431202e+03;

    mState[0 + 7] = mState[1 + 7];
    mState[1 + 7] = mState[2 + 7];
    mState[2 + 7] = mState[3 + 7];
    mState[3 + 7] = mState[4 + 7];
    mState[4 + 7] = mState[5 + 7];
    mState[5 + 7] = mState[6 + 7];
    mState[6 + 7] = (mState[0] + mState[6]) + 6 * (mState[1] + mState[5]) + 15 * (mState[2] + mState[4]) + 20 * mState[3]
                     + (-0.0875254324 * mState[0 + 7]) + (  0.7451953150 * mState[1 + 7])
                     + (-2.6936900468 * mState[2 + 7]) + (  5.3035705416 * mState[3 + 7])
                     + (-6.0154271673 * mState[4 + 7]) + (  3.7396436174 * mState[5 + 7]);

    return mState[6 + 7];
}



