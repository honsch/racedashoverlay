#ifndef FILTER_H
#define FILTER_H

//---------------------------------------------
class Filter
{
  public:
    virtual double NewSample(double in) = 0;

  protected:
    void Fill(double *state, double value, unsigned count);
};

//---------------------------------------------
class Butterworth_4th_10_100 : public Filter
{
  public:
    Butterworth_4th_10_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[10];
};


//---------------------------------------------
class Butterworth_4th_1_100 : public Filter
{
  public:
    Butterworth_4th_1_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[10];
};

//---------------------------------------------
class Bessel_4th_15_100 : public Filter
{
  public:
    Bessel_4th_15_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[10];
};

//---------------------------------------------
class Bessel_4th_10_100 : public Filter
{
  public:
    Bessel_4th_10_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[10];
};

//---------------------------------------------
class Bessel_4th_1_100 : public Filter
{
  public:
    Bessel_4th_1_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[10];
};


//---------------------------------------------
class Bessel_6th_25_100 : public Filter
{
  public:
    Bessel_6th_25_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[14];
};

//---------------------------------------------
class Bessel_6th_20_100 : public Filter
{
  public:
    Bessel_6th_20_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[14];
};
//---------------------------------------------
class Bessel_6th_15_100 : public Filter
{
  public:
    Bessel_6th_15_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[14];
};

//---------------------------------------------
class Bessel_6th_10_100 : public Filter
{
  public:
    Bessel_6th_10_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[14];
};
//---------------------------------------------
class Bessel_6th_5_100 : public Filter
{
  public:
    Bessel_6th_5_100(double fill = 0.0);
    double NewSample(double in);
  private:
    double mState[14];
};

#endif // FILTER_H
