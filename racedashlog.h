#ifndef RACEDASHLOG_H
#define RACEDASHLOG_H

#include <stdint.h>
#include <stdio.h>
#include <limits.h>
#include <vector>

#include "filter.h"

// Current Item names in the log
#define  LOG_GPS_VELOCITY   "GPS Velocity" // In m/s
#define  LOG_GPS_LATITUDE   "GPS Latitude"
#define  LOG_GPS_LONGITUDE  "GPS Longitude"
#define  LOG_GPS_COURSE     "GPS Course"
#define  LOG_GPS_ALTITUDE   "GPS Altitude"
#define  LOG_GPS_HDOP       "GPS HDOP"

#define  LOG_ECU_RPM             "ECU RPM"
#define  LOG_ECU_TPS             "ECU TPS" // %
#define  LOG_ECU_CLT             "ECU CLT" // *C
#define  LOG_ECU_VBAT            "ECU VBat" // VDC
#define  LOG_ECU_MAP             "ECU MAP" // KPa
#define  LOG_ECU_OIL_PRESSURE    "ECU OilPressure" // psi
#define  LOG_ECU_GALLONS_USED    "ECU GallonsUsed"
#define  LOG_ECU_O2              "ECU O2" // afr
#define  LOG_ECU_ADVANCE         "ECU Advance"  // deg
#define  LOG_ECU_EGO             "ECU EGO Correction" // %
#define  LOG_ECU_IAT             "ECU IAT" // *C`
#define  LOG_ECU_SECONDS         "ECU Seconds"
#define  LOG_ECU_TPS_DOT         "ECU TPS Dot" // %/s
#define  LOG_ECU_MAP_DOT         "ECU MAP Dot" // KPa/s
#define  LOG_ECU_INJECTOR_1_PW   "ECU Inj 1 Pulsewidth" //ms
#define  LOG_ECU_INJECTOR_2_PW   "ECU Inj 2 Pulsewidth" //ms
#define  LOG_ECU_INJECTOR_3_PW   "ECU Inj 3 Pulsewidth" //ms
#define  LOG_ECU_INJECTOR_4_PW   "ECU Inj 4 Pulsewidth" //ms
#define  LOG_ECU_WUE             "ECU WEU Correction" // %
#define  LOG_ECU_IAT_CORRECTION  "ECU IAT Correction" // %
#define  LOG_ECU_AFR_TARGET      "ECU AFR Target" // afr
#define  LOG_ECU_SYNC_LOSS_COUNT "ECU Sync Loss Counter"

#define  LOG_IMU_GYRO_X   "IMU Gyro X"
#define  LOG_IMU_GYRO_Y   "IMU Gyro Y"
#define  LOG_IMU_GYRO_Z   "IMU Gyro Z"
#define  LOG_IMU_MAG_X    "IMU Mag X"
#define  LOG_IMU_MAG_Y    "IMU Mag Y"
#define  LOG_IMU_MAG_Z    "IMU Mag Z"
#define  LOG_IMU_ACCEL_X  "IMU Accel X"
#define  LOG_IMU_ACCEL_Y  "IMU Accel Y"
#define  LOG_IMU_ACCEL_Z  "IMU Accel Z"

#define  LOG_LAP_START_UTC   "lapStartUTC"
#define  LOG_LAP_DURATION    "lapDuration"

#define  LOG_ADC_INPUT_0   "ADC Input 00"
#define  LOG_ADC_INPUT_1   "ADC Input 01"
#define  LOG_ADC_INPUT_2   "ADC Input 02"
#define  LOG_ADC_INPUT_3   "ADC Input 03"
#define  LOG_ADC_INPUT_4   "ADC Input 04"
#define  LOG_ADC_INPUT_5   "ADC Input 05"
#define  LOG_ADC_INPUT_6   "ADC Input 06"
#define  LOG_ADC_INPUT_7   "ADC Input 07"
#define  LOG_ADC_INPUT_8   "ADC Input 08"
#define  LOG_ADC_INPUT_9   "ADC Input 09"
#define  LOG_ADC_INPUT_10  "ADC Input 10"
#define  LOG_ADC_INPUT_11  "ADC Input 11"

//--------------------------------------------
class RacedashLog
{
  public:
    RacedashLog(void);
   ~RacedashLog();

    bool Load(const char *fname);

    struct Sample
    {
        double time;
        double value;
    };

    enum LogVariableType { Double, Float, U32, S32, U16, S16, U8, S8 };

    bool            IsValid(void) const { return mTotalSamples > 0; }

    unsigned        NumItems(void)     const { return mLogItems.size(); }
    unsigned        TotalSamples(void) const { return mTotalSamples; }
    unsigned        TotalTimes(void)   const { return mTotalTimeIndices; }

    const char     *ItemName(unsigned itemIndex)  const { return mLogItems[itemIndex].name; }
    const char     *ItemUnits(unsigned itemIndex) const { return mLogItems[itemIndex].units; }
    const char     *ItemTypeName(unsigned itemIndex)  const { return LogTypeNames[mLogItems[itemIndex].type]; }
    LogVariableType ItemType(unsigned itemIndex)  const { return (LogVariableType) mLogItems[itemIndex].type; }
    unsigned        ItemSamples(unsigned itemIndex) const { return mLogItems[itemIndex].samples.size(); }
    const Sample   &ItemSample(unsigned itemIndex, unsigned sampleIndex) const { return mLogItems[itemIndex].samples[sampleIndex]; }

    void            ItemFilter(unsigned itemIndex, Filter &filter);

    // This returns the index of the sample at or latest before time specified
    // returns -1 on empty samples
    int             ItemIndexAtTime(unsigned itemIndex, double time);
    // Interpolate between samples if necessary
    double          ItemValueAtTime(unsigned itemIndex, double time);

    bool            ItemDisplay(unsigned itemIndex) const { return mLogItems[itemIndex].display; }
    void            SetItemDisplay(unsigned itemIndex, bool en) { mLogItems[itemIndex].display = en; }

    // returns itemIndex for item matching "name" or -1 on failure
    int             FindItemByName(const char *name) const;

    double          LogStartTime(void) const { return mLogFirstTime; }


  private:

    void Flush(void);
    bool LoadLogTimeIndex(FILE *inf);

    typedef uint32_t DataLoggerID;
    static const char *LogTypeNames[8];

    //----------------------------------------------------
    struct LogHeaderVariable
    {
        char         name[64];
        char         units[64];
        double       scale;
        uint32_t     type;
        DataLoggerID id;
    };

    //----------------------------------------------------
    struct LogHeader
    {
        uint32_t magic;
        uint32_t version;
        uint32_t numVariables;
        uint32_t _pad;
        LogHeaderVariable _variables[0];
    };

    //----------------------------------------------------
    struct LogTimeIndex
    {
        LogTimeIndex(uint64_t t = 0) : time(t) {}
        uint32_t magic;
        uint32_t numVariables;
        uint64_t time;
        uint8_t  data[0];
    };

    // Internal use
    struct LogItem
    {
        char        *name;
        char        *units;
        bool         display;
        double       scale;
        uint32_t     type;
        DataLoggerID id;
        std::vector<Sample> samples;
        unsigned     lastFoundIndex;
    };

    int FindLogItemByID(DataLoggerID id);
    std::vector<LogItem> mLogItems;
    double   mLogFirstTime;
    unsigned mTotalSamples;
    unsigned mTotalTimeIndices;
    char mLoadedFile[PATH_MAX];

};

#endif // RACEDASHLOG_H
