#include "racedashlog.h"
#include <stdlib.h>
#include <string.h>


#define LOG_HEADER_MAGIC     0x52444C42 // 'RLDB'
#define LOG_TIME_INDEX_MAGIC 0x4C475449 // 'LGTI'

#define LOG_VERSION      0x000002

const char *RacedashLog::LogTypeNames[8] = { "Double", "Float", "U32", "S32", "U16", "S16", "U8", "S8" };

//-----------------------------------------------
RacedashLog::RacedashLog(void) :
    mLogFirstTime(0.0),
    mTotalSamples(0),
    mTotalTimeIndices(0)
{

}

//-----------------------------------------------
RacedashLog::~RacedashLog()
{
    Flush();
}

//-----------------------------------------------
void RacedashLog::Flush(void)
{
printf("Flush called\n");

    mTotalSamples = 0;
    mLogFirstTime = 0;
    while (!mLogItems.empty())
    {
        LogItem &i = mLogItems.back();
        free(i.name);
        free(i.units);
        mLogItems.pop_back();
    }
}

//-----------------------------------------------
bool RacedashLog::Load(const char *fname)
{
    if (fname == NULL) return false;
    if (strlen(fname) < 1) return false;

    // If we've already got this file loaded we're done.
    if (0 == strcmp(fname, mLoadedFile)) return mTotalSamples > 0;

    FILE *inf = fopen(fname, "rt");
    if (inf == NULL) return false;

    // Check the file and make sure the magic is correct
    LogHeader header;
    fread(&header, sizeof(header), 1, inf);
    if (header.magic != LOG_HEADER_MAGIC)
    {
        printf("Log magic mismatch, read 0x%08X shd have been 0x%08X\n", header.magic, LOG_HEADER_MAGIC);
        return false;
    }

    if (header.version != LOG_VERSION)
    {
        printf("Log version does not match should be %d, was %d\n", LOG_VERSION, header.version);
        return false;
    }

    Flush();
    strcpy(mLoadedFile, fname);

    // Load the variable info
    unsigned a;
    for (a = 0; a < header.numVariables; a++)
    {
        LogHeaderVariable var;
        fread(&var, sizeof(var), 1, inf);
        LogItem item;
        item.name    = strdup(var.name);
        item.units   = strdup(var.units);
        item.scale   = 1.0 / var.scale;
        item.type    = var.type;
        item.id      = var.id;
        item.display = true;
        item.lastFoundIndex = 0;
        mLogItems.push_back(item);
    }

    // load the samples
    bool done = false;
    while (!feof(inf) && !done)
    {
        uint32_t magic;
        fread(&magic, sizeof(magic), 1, inf);
        fseek(inf, -sizeof(magic), SEEK_CUR);

        switch(magic)
        {
            case LOG_TIME_INDEX_MAGIC:
                done = !LoadLogTimeIndex(inf);
                break;

            default:
                printf("Got a bad Magic: 0x%08X\n", magic);
                done = true;
                break;
        }
    }

    fclose(inf);

    mLogFirstTime = mLogItems[0].samples[0].time;
    for (a = 1; a < mLogItems.size(); a++)
    {
        if (!mLogItems[a].samples.empty())
        {
            double t = mLogItems[a].samples[0].time;
            if (t < mLogFirstTime) mLogFirstTime = t;
        }
    }

    return true;
}

//-----------------------------------------------
bool RacedashLog::LoadLogTimeIndex(FILE *inf)
{
    ++mTotalTimeIndices;
    struct LogTimeIndex lti;
    fread(&lti, sizeof(lti), 1, inf);

    unsigned a;
    for (a = 0; a < lti.numVariables; a++)
    {
        DataLoggerID id;
        fread(&id, sizeof(id), 1, inf);

        int index = FindLogItemByID(id);
        if (index < 0) return false;

        LogItem &li = mLogItems[index];
        double out = 0;
        switch (li.type)
        {
            case Double:
            {
                double v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case Float:
            {
                float v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case U32:
            {
                uint32_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case S32:
            {
                int32_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case U16:
            {
                uint16_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case S16:
            {
                int16_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case U8:
            {
                uint8_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
            case S8:
            {
                int8_t v;
                fread(&v, sizeof(v), 1, inf);
                out = (double) v;
                break;
            }
        }

        Sample s;
        s.time = lti.time * 0.0001;
        s.value = out * li.scale;
        if (s.time >1000000.0)
        {
            li.samples.push_back(s);
            ++mTotalSamples;
            if (s.time < mLogFirstTime) mLogFirstTime = s.time;
        }
        else
        {
            printf("Sample dropped on %s, invalid time %f\n", li.name, s.time);
        }
    }
    return true;
}

//----------------------------------------------------
int RacedashLog::FindLogItemByID(DataLoggerID id)
{
    unsigned a;
    for (a = 0; a < mLogItems.size(); a++)
    {
        if (mLogItems[a].id == id) return (int) a;
    }
    return -1;
}

//----------------------------------------------------
int RacedashLog::ItemIndexAtTime(unsigned itemIndex, double time)
{
    if (mLogItems[itemIndex].samples.empty()) return -1;

    const std::vector<Sample> &samples = mLogItems[itemIndex].samples;
    double first = samples[0].time;
    double last  = samples[samples.size() - 1].time;

    if (first >= time) return 0;
    if (last  <= time) return samples.size() - 1;

    // because samples are more-or-less equally spaced we can use an heuristic to find where we should start looking
    //double range =  last - first;
    //double scale = range / (double) samples.size();

    unsigned index = mLogItems[itemIndex].lastFoundIndex; //(unsigned) ((time - first) * scale);

    while ((index < (samples.size() - 2)) && (samples[index].time < time) && (samples[index + 1].time <= time)) ++index;

    while ((index != 0) && (samples[index].time > time)) --index;
    mLogItems[itemIndex].lastFoundIndex = index;
    return index;
}

//----------------------------------------------------
// Interpolates!!!!
double RacedashLog::ItemValueAtTime(unsigned itemIndex, double time)
{
    const std::vector<Sample> &samples = mLogItems[itemIndex].samples;

    int index = ItemIndexAtTime(itemIndex, time);
    if (index == 0) return samples[index].value;
    if (index >= samples.size() - 1) return samples[index].value;

    double pre = samples[index].time;
    double post  = samples[index + 1].time;
    double t = (time - pre) / (post - pre);

    pre = samples[index].value;
    post = samples[index + 1].value;
    return pre + (t * (post - pre));
}

//----------------------------------------------------
void RacedashLog::ItemFilter(unsigned itemIndex, Filter &filter)
{
    std::vector<Sample> &samples = mLogItems[itemIndex].samples;
    unsigned count = samples.size();
    unsigned a;

    for(a = 0; a < count ; a++)
    {
        double fv = filter.NewSample(samples[a].value);
        samples[a].value = fv;
    }
}

//----------------------------------------------------
int RacedashLog::FindItemByName(const char *name) const
{
    if (name == NULL) return -1;
    int a;
    for (a = 0; a < mLogItems.size(); a++)
    {
        if (0 == strcmp(name, mLogItems[a].name)) return a;
    }

    return -1;
}
