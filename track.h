#include <stdint.h>
#include <limits.h>
#include <vector>

#ifndef TRACK_H
#define TRACK_H
//-----------------------------------------------------
struct TrackCoord
{
    double latitude;
    double longitude;
    double altitude;
};

//-----------------------------------------------------
struct TrackSector : public TrackCoord
{
    unsigned index;
    char name[64];
};

//-----------------------------------------------------
class Track
{
  public:

    Track(void);
   ~Track();

    bool Load(const char *fname);
    void Unload(void);

    const char *Name(void) const { return mName; }
    bool HasSectors(void) const { return !mSectors.empty(); }

    const std::vector<TrackCoord>  &InnerSamples(void) const { return mInner; }
    const std::vector<TrackCoord>  &OuterSamples(void) const { return mOuter; }
    const std::vector<TrackSector> &Sectors(void)      const { return mSectors; }
    const TrackCoord               &StartFinish(void)  const { return mStartFinish; }

    const TrackSector &FindSector(unsigned index) const;
    const TrackSector &Sector(unsigned index) const;
    const unsigned     NumSectors(void) const { return mSectors.size(); }
  private:

    unsigned ExtractStrings(const char **strings, unsigned maxStrings, char *line);

    char *mLoadedFile;
    char *mName;

    std::vector<TrackCoord>  mInner;
    std::vector<TrackCoord>  mOuter;
    std::vector<TrackSector> mSectors;
    TrackCoord mStartFinish;
};

#endif // TRACK_H
