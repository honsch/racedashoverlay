#include "track.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


static TrackSector sInvalid;

//-----------------------------------------------------
Track::Track(void)
{
    mLoadedFile = NULL;
    mName = NULL;
    Unload();
    memset(&sInvalid, 0, sizeof(sInvalid));
}

//-----------------------------------------------------
Track::~Track()
{

}

//-----------------------------------------------------
bool Track::Load(const char *fname)
{
    if (fname == NULL) return false;
    if (strlen(fname) < 1) return false;

    if ((mLoadedFile != NULL) && (0 == strcmp(mLoadedFile, fname)))
    {
        return true;
    }


    Unload();

    char line[1024];
    char rawLine[1024];

    mLoadedFile = strdup(fname);

    FILE *inf = fopen(fname, "rt");
    if (inf == NULL)
    {
        printf("Cannot open %s for reading.\n", fname);
        return false;
    }

    const char *strings[32];
    unsigned currentLine = 0;

    enum LoadState { Unknown, Inner, Outer, Sectors, Name, StartFinish } loadState;
    loadState = Unknown;

    while (!feof(inf))
    {
        ++currentLine;
        char *lineRet = fgets(line, 1023, inf);
        if (lineRet == NULL) continue;
        strcpy(rawLine, line);

        unsigned numStrings = ExtractStrings(strings, 32, line);

        if (numStrings < 1) continue;

        if (strings[0][0] == '#') // Comment line, ignore
        {
            continue;
        }
        else if (0 == strcasecmp("[inner]", strings[0]))
        {
            loadState = Inner;
            continue;
        }
        else if (0 == strcasecmp("[outer]", strings[0]))
        {
            loadState = Outer;
            continue;
        }
        else if (0 == strcasecmp("[sectors]", strings[0]))
        {
            loadState = Sectors;
            continue;
        }
        else if (0 == strcasecmp("[name]", strings[0]))
        {
            loadState = Name;
            continue;
        }
        else if (0 == strcasecmp("[startfinish]", strings[0]))
        {
            loadState = StartFinish;
            continue;
        }

        TrackCoord tc;
        TrackSector ts;
        switch (loadState)
        {
            case Inner:
            case Outer:
            case StartFinish:
                if (numStrings == 3)
                {
                    tc.longitude = atof(strings[0]);
                    tc.latitude  = atof(strings[1]);
                    tc.altitude  = atof(strings[2]);
                }
                else
                {
                    if (strlen(rawLine) > 3) printf("Loading track %s line %d, wrong number of strings: %d, should be 3\n", mLoadedFile, currentLine, numStrings);
                    continue;
                }
                break;

            case Sectors:
                if (numStrings == 5)
                {
                    strncpy(ts.name, strings[1], 64);
                    ts.index     = atoi(strings[0]);
                    ts.longitude = atof(strings[2]);
                    ts.latitude  = atof(strings[3]);
                    ts.altitude  = atof(strings[4]);
                    //printf("Found sector %d  %s %f %f %f\n", ts.index, ts.name, ts.longitude, ts.latitude, ts.altitude);
                }
                else
                {
                    if (strlen(rawLine) > 3) printf("Loading track %s line %d, wrong number of strings: %d, should be 5\n", mLoadedFile, currentLine, numStrings);
                    continue;
                }
                break;

            case Name:
                {
                    unsigned len = strlen(rawLine);
                    unsigned a = 0;
                    while (a < len)
                    {
                        if (rawLine[a] < 32) rawLine[a] = 0;
                        ++a;
                    }

                    if (strlen(rawLine) > 0)
                    {
                        if (mName != NULL) free(mName);
                        mName = strdup(rawLine);
                    }
                }
                break;


            default:
                continue;
                break;
        }

        switch (loadState)
        {
            case Inner:
                mInner.push_back(tc);
                break;

            case Outer:
                mOuter.push_back(tc);
                break;

            case StartFinish:
                mStartFinish = tc;
                break;
            case Sectors:
                mSectors.push_back(ts);
                break;
            default:
                break;
        }
    }

    // Is any required data missing?
    if ((mName == NULL) || (mInner.size() == 0) || (mOuter.size() == 0) || (mStartFinish.altitude < -500.0))
    {
        if (mName == NULL) printf("Track %s (%s) is missing [name], dropped\n", mName, mLoadedFile);
        if (mInner.size() == 0) printf("Track %s (%s) is missing [inner] path, dropped\n", mName, mLoadedFile);
        if (mOuter.size() == 0) printf("Track %s (%s) is missing [outer] path, dropped\n", mName, mLoadedFile);
        if (mStartFinish.altitude < -500.0) printf("Track %s (%s) is missing [startfinish] point, dropped\n", mName, mLoadedFile);

        Unload();
        return false;
    }

    printf("Loaded track %s (%s), %d inner, %d outer, %d sectors\n", mName, mLoadedFile, (int)mInner.size(), (int)mOuter.size(), (int)mSectors.size());

    return true;
}

//-----------------------------------------------------
void Track::Unload(void)
{
    if (mLoadedFile != NULL) free(mLoadedFile);
    if (mName != NULL)       free(mName);
    mLoadedFile = NULL;
    mName = NULL;
    mInner.clear();
    mOuter.clear();
    mSectors.clear();
    mStartFinish.latitude  =  0.0;
    mStartFinish.longitude =  0.0;
    mStartFinish.altitude  = -999.0;
}

//---------------------------------------------------------
unsigned Track::ExtractStrings(const char **strings, unsigned maxStrings, char *line)
{
    unsigned a;
    for (a = 0; a < maxStrings; a++) strings[a] = NULL;

    if (line == NULL) return 0;

    unsigned len = strlen(line);
    if (len == 0) return 0;

    // strip ending CR/LF from string
    while ((len > 0) && ((line[len - 1] == '\n') || (line[len - 1] == '\r')))
    {
        --len;
        line[len] = 0;
    }

    unsigned numStrings = 0;
    unsigned pos = 0;
    while (pos < len)
    {
        strings[numStrings] = &line[pos];
        ++numStrings;

        // Advance past the text of the entry, possibly blank!
        bool inQuotes = false;
        while (pos < len)
        {
            if (line[pos] == '"') inQuotes = !inQuotes;
            if (!inQuotes && (line[pos] == ',')) break;
            ++pos;
        }

        if (pos >= len) break;

        line[pos] = 0;
        ++pos; // move past the previous string
    }

    return numStrings;
}

//-------------------------------------------------------
const TrackSector &Track::FindSector(unsigned index) const
{
    unsigned a;
    for (a = 0; a < mSectors.size(); a++)
    {
        if (mSectors[a].index == index) return mSectors[a];
    }
    return sInvalid;
}

//-------------------------------------------------------
const TrackSector &Track::Sector(unsigned index) const
{
    if (index < mSectors.size()) return mSectors[index];
    return sInvalid;
}
