#include "frei0r.hpp"
#include "frei0r_cairo.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <cairo.h>

#include "racedashlog.h"
#include "track.h"

#define TAU (2.0 * 3.141592654)
#define MS_TO_MPH 2.23694

//----------------------------------------------------------------------------
class RaceDashOverlay : public frei0r::filter
{
  public:
    RaceDashOverlay(unsigned int width, unsigned int height);
   ~RaceDashOverlay();

    virtual void update(double time, uint32_t *out, const uint32_t *in);

  private:

    // My Parameters
    f0r_param_position mLapPos;
    f0r_param_position mLapTimerPos;
    f0r_param_position mSpeedPos;
    f0r_param_position mSectorPos;
    f0r_param_color    mLapColor;
    f0r_param_color    mLapTimerColor;
    f0r_param_color    mSpeedColor;
    f0r_param_color    mSpeedGraphColor;
    f0r_param_color    mSectorColor;

    bool mLogLoaded;
    bool mTrackLoaded;

    bool mShowSpeedGraph;
    bool mShowGMeter;
    bool mShowTelemetryTime;
    bool mShowTrack;
    bool mShowSectorMarkers;

    std::string      mTelemetryFilename;
    std::string      mTrackFilename;

    double           mTimeOffset;
    double           mTrackScale;
    double           mTrailDuration;

    double           mLapTime;

    // Log item indices for named data
    int mLatIndex;
    int mLongIndex;
    int mSpeedIndex;
    int mHeadingIndex;
    int mAccelXIndex;
    int mAccelYIndex;
    int mTPSIndex;
    int mLapStartIndex;

    RacedashLog mLog;
    Track     mTrack;

    void DrawSpeedGraph(cairo_t *cr, double x, double y, double w, double h, double maxSpeed, double timeWidth, double timeCenter);
    void DrawGMeter(cairo_t *cr, double x, double y, double r, double maxG, double startTime, double endTime);
    void DrawTrack(cairo_t *cr, double x, double y, double w, double h, double scale, double startTime, double endTime);

    //----------------------------------------------------------------------------
    void LatLonToCoord(double &x, double &y, double scale, double heading, double latRef, double lonRef, double lat, double lon)
    {
        double tx = scale *  (lon - lonRef);
        double ty = scale * -(lat - latRef);

        double headingRad = (360.0 - heading) * (3.141592654 / 180.0);
        double s, c;
        sincos(headingRad, &s, &c);

        x = (tx * c) - (ty * s);
        y = (tx * s) + (ty * c);
    }
};

//----------------------------------------------------------------------------
RaceDashOverlay::RaceDashOverlay(unsigned int width, unsigned int height)
{
    mLapPos.x = 0.05;
    mLapPos.y = 0.05;

    mLapColor.r = 0.9;
    mLapColor.g = 0.9;
    mLapColor.b = 0.9;

    mLapTimerPos.x = 0.05;
    mLapTimerPos.y = 0.1;

    mLapTimerColor.r = 0.9;
    mLapTimerColor.g = 0.9;
    mLapTimerColor.b = 0.9;

    mSpeedPos.x = 0.85;
    mSpeedPos.y = 0.05;

    mSpeedColor.r = 0.0;
    mSpeedColor.g = 1.0;
    mSpeedColor.b = 1.0;

    mShowSpeedGraph = true;
    mSpeedGraphColor.r = 1.0;
    mSpeedGraphColor.g = 1.0;
    mSpeedGraphColor.b = 1.0;

    mSectorPos.x = 0.35;
    mSectorPos.y = 0.05;

    mSectorColor.r = 0.9;
    mSectorColor.g = 0.9;
    mSectorColor.b = 0.9;

    mShowGMeter = true;

    mShowTelemetryTime = false;

    mShowSectorMarkers = false;

    mTelemetryFilename.clear();
    mTelemetryFilename = "telemetry.txt";

    mTrackFilename.clear();
    mTrackFilename = "track.txt";
    mTrackScale = 12.0;
    mTrailDuration = 5.0;
    mTimeOffset = 0.0;

    mLogLoaded = false;
    mTrackLoaded = false;


    mLapTime = 0.0;

    mLatIndex      = -1;
    mLongIndex     = -1;
    mSpeedIndex    = -1;
    mHeadingIndex  = -1;
    mAccelXIndex   = -1;
    mAccelYIndex   = -1;
    mTPSIndex      = -1;
    mLapStartIndex = -1;

    register_param(mTelemetryFilename,   "CSV Filename", "Choose the telemetry filename");
    register_param(mTimeOffset, "Telemetry Time Offset", "Sync the telemetry to the video (in seconds)");

    register_param(mTrackFilename, "Track Filename", "Choose the track filename");
    register_param(mShowTrack,     "Show Track", "Show the track?");
    register_param(mTrackScale,    "Track Scale", "Zoom level of track");
    register_param(mTrailDuration, "Trail Duration", "Trail length for G meter and track overhead");

    register_param(mLapPos,   "Lap Counter Pos", "Where is the lap counter?");
    register_param(mLapColor, "Lap Counter Color", "Color of lap counter");

    register_param(mLapTimerPos,   "Lap Timer Pos", "Where is the lap timer?");
    register_param(mLapTimerColor, "Lap Timer Color", "Color of lap timer");

    register_param(mSpeedPos,   "Speed Pos", "Where is the speedometer?");
    register_param(mSpeedColor, "Speed Color", "Color of speedometer");

    register_param(mSectorPos,   "Sector Timer Pos", "Where is the sector timer?");
    register_param(mSectorColor, "Sector Timer Color", "Color of sector timer");
    register_param(mShowSectorMarkers, "Show Sector Markers", "Show sector markers?");

    register_param(mShowSpeedGraph,  "Show Speed Graph", "Show the speed graph");
    register_param(mSpeedGraphColor, "Speed Graph color", "Color of speed graph data");

    register_param(mShowGMeter,  "Show G Meter", "Show the G meter");
    register_param(mShowTelemetryTime,  "Show Telemetry Time", "Show the time used for sourcing the telemetry overlay");

}

//----------------------------------------------------------------------------
RaceDashOverlay::~RaceDashOverlay()
{
}

//----------------------------------------------------------------------------
void RaceDashOverlay::update(double time, uint32_t *out, const uint32_t *in)
{
    int stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32,  width);
    cairo_surface_t *surface = cairo_image_surface_create_for_data((unsigned char *)out, CAIRO_FORMAT_ARGB32, width, height, stride);
    cairo_t *cr = cairo_create(surface);

    // Just copy input to output.
    // I'll overlay the data later
    cairo_surface_t *src = cairo_image_surface_create_for_data ((unsigned char*)in, CAIRO_FORMAT_ARGB32, width, height, stride);
    cairo_set_source_surface(cr, src, 0, 0);
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_paint(cr);
    cairo_surface_destroy(src);

    cairo_set_operator (cr, CAIRO_OPERATOR_OVER);

    char s[250];

    mLogLoaded = mLog.Load(mTelemetryFilename.c_str());

    if (mShowTrack)
    {
        mTrackLoaded = mTrack.Load(mTrackFilename.c_str());
    }

    cairo_select_font_face(cr, "serif", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);

    if (!mLogLoaded)
    {
        sprintf(s, "Error: Cant load '%s'", mTelemetryFilename.c_str());
        cairo_set_font_size(cr, 32.0);
        cairo_set_source_rgb(cr, 0.0, 0.0, 1.0);
        cairo_move_to(cr, 10.0, 140.0);
        cairo_show_text(cr, s);
    }
    else
    {
        if (mLatIndex      < 0) mLatIndex      = mLog.FindItemByName(LOG_GPS_LATITUDE);
        if (mLongIndex     < 0) mLongIndex     = mLog.FindItemByName(LOG_GPS_LONGITUDE);
        if (mSpeedIndex    < 0) mSpeedIndex    = mLog.FindItemByName(LOG_GPS_VELOCITY);
        if (mHeadingIndex  < 0) mHeadingIndex  = mLog.FindItemByName(LOG_GPS_COURSE);
        if (mTPSIndex      < 0) mTPSIndex      = mLog.FindItemByName(LOG_ECU_TPS);
        if (mLapStartIndex < 0) mLapStartIndex = mLog.FindItemByName(LOG_LAP_START_UTC);

        if (mAccelXIndex < 0)
        {
            mAccelXIndex = mLog.FindItemByName(LOG_IMU_ACCEL_X);
            if (mAccelXIndex >= 0)
            {
                Bessel_6th_5_100 filter1, filter2;
                mLog.ItemFilter(mAccelXIndex, filter1);
                mLog.ItemFilter(mAccelXIndex, filter2);
            }
        }

        if (mAccelYIndex < 0)
        {
            mAccelYIndex = mLog.FindItemByName(LOG_IMU_ACCEL_Y);
            if (mAccelYIndex >= 0)
            {
                Bessel_6th_5_100 filter1, filter2;
                mLog.ItemFilter(mAccelYIndex, filter1);
                mLog.ItemFilter(mAccelYIndex, filter2);
            }
        }

        //if (mTrackLoaded && mTrack.HasSectors())
        //{
        //    mTelemetry.UpdateSectors(mTrack);
        //}

        double telemTime = time + mLog.LogStartTime() + mTimeOffset;

        double speed = 0.0;
        if (mSpeedIndex >= 0) speed = mLog.ItemValueAtTime(mSpeedIndex, telemTime) * MS_TO_MPH;

        if (1)
        {
            // Lap counter & lap timer
            int lap = 0;
            double lapTime = 0.0;
            if (mLapStartIndex >= 0)
            {
                int sample = mLog.ItemIndexAtTime(mLapStartIndex, telemTime);
                if (sample >= 0)
                {
                    lap = mLog.ItemSample(mLapStartIndex, sample).value;
                    lapTime = telemTime - mLog.ItemSample(mLapStartIndex, sample).time;
                }
                if (lapTime >= 5.0) mLapTime = lapTime;
            }
            bool showLastLap = false;
            if ((lap > 1) && (lapTime < 5.0))
            {
                lapTime = mLapTime;
                sprintf(s, "Lap: %d", lap - 1);
                showLastLap = true;
            }
            else
            {
                sprintf(s, "Lap: %d", lap);
            }

            cairo_set_source_rgb(cr, mLapColor.r, mLapColor.g, mLapColor.b);
            cairo_set_font_size(cr, 64.0);
            cairo_move_to(cr, mLapPos.x * width, mLapPos.y * height);
            cairo_show_text(cr, s);

            unsigned min = (unsigned) lapTime / 60.0;
            double secs = lapTime - (min * 60.0);

            if (showLastLap)
            {
                cairo_set_source_rgb(cr, 0.0, 1.0, 0.0);
                sprintf(s, "%02d:%06.3f", min, secs);
            }
            else
            {
                cairo_set_source_rgb(cr, mLapTimerColor.r, mLapTimerColor.g, mLapTimerColor.b);
                sprintf(s, "%02d:%04.1f", min, secs);
            }

            cairo_move_to(cr, mLapTimerPos.x * width, mLapTimerPos.y * height);
            cairo_show_text(cr, s);

            // speedo
            sprintf(s, "%3dmph", (int)speed);
            cairo_set_source_rgb(cr, mSpeedColor.r, mSpeedColor.g, mSpeedColor.b);
            cairo_move_to(cr, mSpeedPos.x * width, mSpeedPos.y * height);
            cairo_show_text(cr, s);

            // Sector timer

#if 0
            if (mTelemetry.SectorsGood())
            {
                double sectorTime = pre.mSectorTime;
                min = (unsigned) sectorTime / 60.0;
                secs = sectorTime - (min * 60.0);
                if (sectorTime > 5.0)
                {
                    sprintf(s, "Sector %d %02d:%04.1f", pre.mSectorIndex, min, secs);
                    cairo_set_source_rgb(cr, mSectorColor.r, mSectorColor.g, mSectorColor.b);
                    cairo_move_to(cr, mSectorPos.x * width, mSectorPos.y * height);
                    cairo_show_text(cr, s);
                }
                else
                {
                    unsigned index = mTelemetry.FindClosestBefore(telemTime);
                    while ((index > 0) && (mTelemetry.Sample(index).mSectorIndex == pre.mSectorIndex)) --index;

                    sectorTime = mTelemetry.Sample(index).mSectorTime;
                    min = (unsigned) sectorTime / 60.0;
                    secs = sectorTime - (min * 60.0);
                    sprintf(s, "Sector %d %02d:%06.3f", mTelemetry.Sample(index).mSectorIndex, min, secs);
                    cairo_set_source_rgb(cr, 0.0, 1.0, 0.0);
                    cairo_move_to(cr, mSectorPos.x * width, mSectorPos.y * height);
                    cairo_show_text(cr, s);
                }
            }
#endif
        }

        if (mShowSpeedGraph)
        {
            DrawSpeedGraph(cr, 1050, 800, 600, 275, 120.0, 10.0, telemTime);
        }

        if (mShowGMeter)
        {
            DrawGMeter(cr, 1790, 935, 125, 1.5, telemTime - mTrailDuration, telemTime);
        }

        if (mTrackLoaded)
        {
            DrawTrack(cr, 100, 800, 275, 275, mTrackScale, telemTime - mTrailDuration, telemTime);
        }

        if (mShowTelemetryTime)
        {
            sprintf(s, "%.2f  %.2f", telemTime - mLog.LogStartTime(), mLog.LogStartTime());
            cairo_set_operator (cr, CAIRO_OPERATOR_OVER);
            cairo_set_font_size(cr, 32.0);
            cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
            cairo_move_to(cr, 0, height - 32.0);
            cairo_show_text(cr, s);
        }
    }

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
}

//----------------------------------------------------------------------------
void RaceDashOverlay::DrawSpeedGraph(cairo_t *cr, double _x, double _y, double _w, double _h, double maxSpeed, double timeWidth, double timeCenter)
{
    if (mSpeedIndex < 0) return;

    if (_x > width)  return;
    if (_y > height) return;
    if ((_x + _w) > width)  _w = width  - _x;
    if ((_y + _h) > height) _h = height - _y;

    double yScale = ((double) _h) / -maxSpeed;
    double xScale = ((double) _w) / timeWidth;
    double tScale = timeWidth / ((double) _w);

    double halfTime  = timeWidth / 2.0;
    double startTime = timeCenter - halfTime;
    double endTime   = timeCenter + halfTime;

    unsigned numSamples = mLog.ItemSamples(mSpeedIndex);
    if (mLog.ItemSample(mSpeedIndex, 0).time > endTime) return;
    if (mLog.ItemSample(mSpeedIndex, numSamples - 1).time < startTime) return;

    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);

    // Fill the background
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, _x, _y, _w, _h);
    cairo_fill_preserve(cr);
    cairo_set_source_rgb(cr, 0.25, 0.25, 0.25);
    cairo_stroke(cr);

    // Draw the vertical line
    cairo_move_to(cr, _x + (_w / 2.0), _y);
    cairo_line_to(cr, _x + (_w / 2), _y + height);
    cairo_stroke(cr);

    // Draw 10mph lines
    cairo_set_source_rgb(cr, 0.0, 0.25, 0.25);
    double mph;
    for (mph = 10.0; mph < maxSpeed; mph += 10.0)
    {
        double y = (_y + _h) + (yScale * mph);
        cairo_move_to(cr, _x,      y);
        cairo_line_to(cr, _x + _w, y);
    }
    cairo_stroke(cr);

    double speed = mLog.ItemValueAtTime(mSpeedIndex, timeCenter) * MS_TO_MPH;
    char s[10];
    sprintf(s, "%3d", (int) speed);
    cairo_set_operator (cr, CAIRO_OPERATOR_OVER);
    cairo_set_font_size(cr, 32.0);
    cairo_set_source_rgb(cr, mSpeedColor.r, mSpeedColor.g, mSpeedColor.b);
    cairo_move_to(cr, _x, _y + 32.0);
    cairo_show_text(cr, s);

    int index = mLog.ItemIndexAtTime(mSpeedIndex, startTime);
    if (index < 1) index = 1;
    bool first = true;

    cairo_set_source_rgb(cr, mSpeedColor.r, mSpeedColor.g, mSpeedColor.b);
    while (mLog.ItemSample(mSpeedIndex, index).time < endTime)
    {
        double x = _x + ((mLog.ItemSample(mSpeedIndex, index).time - startTime) * xScale);
        double y = (_y + _h) + (yScale * mLog.ItemSample(mSpeedIndex, index).value * MS_TO_MPH);
        ++index;
        if (first) cairo_move_to(cr, x, y);
        else       cairo_line_to(cr, x, y);
        first = false;
        if (index >= numSamples) break;
    }
    cairo_stroke(cr);
}

//---------------------------------------------------------------------------------------
void RaceDashOverlay::DrawGMeter(cairo_t *cr, double x, double y, double r, double maxG, double startTime, double endTime)
{
    if ((mAccelXIndex < 0) || (mAccelYIndex < 0)) return;

    if (x > width)  return;
    if (y > height) return;
    if ((x + r) > width)  r = width  - x;
    if ((y + r) > height) r = height - y;

    double scale = r / maxG;

    // Fill the background
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_arc(cr, x, y, r, 0.0, TAU);
    cairo_fill_preserve(cr);
    cairo_set_source_rgb(cr, 0.25, 0.25, 0.25);
    cairo_stroke(cr);

    // Draw G circles
    double g;
    bool half = true;
    for (g = 0.5; g < maxG; g += 0.5)
    {
        if (half) cairo_set_source_rgb(cr, 0.25, 0.25, 0.25);
        else      cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
        half = !half;
        cairo_arc(cr, x, y, g * scale, 0.0, TAU);
        cairo_stroke(cr);
    }

    // Draw axis lines
    cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
    cairo_move_to(cr, x - r, y);
    cairo_line_to(cr, x + r, y);
    cairo_move_to(cr, x, y - r);
    cairo_line_to(cr, x, y + r);
    cairo_stroke(cr);

    cairo_set_operator(cr, CAIRO_OPERATOR_OVER);

    // Draw values
    cairo_set_font_size(cr, 20.0);
    cairo_set_source_rgb(cr, 1.0, 1.0, 0.0);

    double accelX = mLog.ItemValueAtTime(mAccelXIndex, endTime) * 0.1;
    double accelY = mLog.ItemValueAtTime(mAccelYIndex, endTime) * 0.1;

    char s[32];
    double gX = fabs(accelX);
    double gY = fabs(accelY);
    double gRMS = sqrt((gX * gX) + (gY * gY));

    sprintf(s, "X: %3.1f", gX);
    cairo_move_to(cr, (x - r), (y - r) + 20.0);
    cairo_show_text(cr, s);

    sprintf(s, "Y: %3.1f", gY);
    cairo_move_to(cr, (x + r) - 60.0, (y - r) + 20.0);
    cairo_show_text(cr, s);

    sprintf(s, "RMS: %3.1f", gRMS);
    cairo_move_to(cr, (x - r), (y + r));
    cairo_show_text(cr, s);


    // Draw history
    bool first = true;
    double now = startTime;
    cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 0.33);
    while (now <= endTime)
    {
        double gx = x + (mLog.ItemValueAtTime(mAccelXIndex, now) * 0.1 * scale);
        double gy = y + (mLog.ItemValueAtTime(mAccelYIndex, now) * 0.1 * -scale);
        now += 0.05;
        if (first) cairo_move_to(cr, gx, gy);
        else       cairo_line_to(cr, gx, gy);
        first = false;
    }
    cairo_stroke(cr);

    // Draw larger dot for 'now'
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_set_source_rgb(cr, 0.0, 1.0, 0.0);
    double gx = x + (accelX * scale);
    double gy = y + (accelY * -scale);
    cairo_arc(cr, gx, gy, 4.0, 0.0, TAU);
    cairo_fill(cr);
}

//---------------------------------------------------------------------------------------
void RaceDashOverlay::DrawTrack(cairo_t *cr, double x, double y, double w, double h, double scale, double startTime, double endTime)
{
    if ((mLatIndex < 0) || (mLongIndex < 0) || (mHeadingIndex < 0)) return;

    if (x > width)  return;
    if (y > height) return;
    if ((x + w) > width)  w = width  - x;
    if ((y + h) > height) h = height - y;

    double xC = x + (w / 2);
    double yC = y + (h / 2);

    // Make the numbers in the overlay reasonable
    scale *= 20000.0;

    const std::vector<TrackCoord> &inner = mTrack.InnerSamples();
    const std::vector<TrackCoord> &outer = mTrack.OuterSamples();

    cairo_save(cr);

    // Only draw in our small area
    cairo_rectangle(cr, x, y, w, h);
    cairo_clip(cr);

    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);

    double nowLat     = mLog.ItemValueAtTime(mLatIndex, endTime);
    double nowLong    = mLog.ItemValueAtTime(mLongIndex, endTime);
    double nowHeading = mLog.ItemValueAtTime(mHeadingIndex, endTime);

    int a;
    for (a = 0; a < (int)outer.size(); a++)
    {
        double tX, tY;
        LatLonToCoord(tX, tY, scale, nowHeading, nowLat, nowLong, outer[a].latitude, outer[a].longitude);
        if (a == 0) cairo_move_to(cr, xC + tX, yC + tY);
        else        cairo_line_to(cr, xC + tX, yC + tY);
    }

    for (a = (int)inner.size() - 1; a >= 0; a--)
    {
        double tX, tY;
        LatLonToCoord(tX, tY, scale, nowHeading, nowLat, nowLong, inner[a].latitude, inner[a].longitude);
        cairo_line_to(cr, xC + tX, yC + tY);
    }

    cairo_set_source_rgb(cr, 0.20, 0.2, 0.2);
    cairo_fill_preserve(cr);

    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_stroke(cr);

    // Draw history line
    cairo_set_source_rgb(cr, 0.5, 0.5, 0.5);
    bool first = true;
    double t;
    for (t = startTime ; t <= endTime; t += 0.1)
    {
        double tLat  = mLog.ItemValueAtTime(mLatIndex, t);
        double tLong = mLog.ItemValueAtTime(mLongIndex, t);
        double tX, tY;
        LatLonToCoord(tX, tY, scale, nowHeading, nowLat, nowLong, tLat, tLong);
        if (first) cairo_move_to(cr, xC + tX, yC + tY);
        else       cairo_line_to(cr, xC + tX, yC + tY);
        first = false;
    }
    cairo_stroke(cr);

    // Draw Sectors

    if (mShowSectorMarkers)
    {
        for (a = 0; a < mTrack.NumSectors(); a++)
        {
            const TrackSector &ts = mTrack.Sector(a);
            double tX, tY;
            LatLonToCoord(tX, tY, scale, nowHeading, nowLat, nowLong, ts.latitude, ts.longitude);
            cairo_set_source_rgb(cr, 0.3, 0.3, 0.3);
            cairo_arc(cr, tX + xC, tY + yC, 20.0, 0.0, TAU);
            cairo_fill_preserve(cr);

            cairo_set_source_rgb(cr, 0.80, 0.8, 0.8);
            cairo_stroke(cr);
        }
    }

    // Draw us
    cairo_set_source_rgb(cr, 0.0, 1.0, 0.0);
    cairo_arc(cr, xC, yC, 4.0, 0.0, TAU);
    cairo_fill(cr);

    cairo_restore(cr);
}

//---------------------------------------------------------------------------------------
frei0r::construct<RaceDashOverlay> plugin("Racedash Overlay",
                                           "A filter that overlays racedash log data",
                                           "Eric Honsch",
                                           0, 1, // Major, minor version numbers
                                           F0R_COLOR_MODEL_RGBA8888);
